package main

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func func1(args []string) {
	var s, sep string
	for i := 1; i < len(args); i++ {
		s += sep + args[i]
		sep = " "

	}
	fmt.Println(s)

}

func func2(args []string) {
	fmt.Println(strings.Join(args[1:], " "))
}

func main() {
	args := os.Args
	t1 := time.Now()
	func1(args)
	t2 := time.Now()
	func2(args)
	t3 := time.Now()
	fmt.Println(t2.Sub(t1))
	fmt.Println(t3.Sub(t2))

}
