package tempconv

// CToF converts a Celsius temperature to Fahrenheit.
func CToF(c Celsius) Fahrenheit { return Fahrenheit(c*9/5 + 32) }

// FToC converts a Fahrenheit temperature to Celsius.
func FToC(f Fahrenheit) Celsius { return Celsius((f - 32) * 5 / 9) }

// KToC converts a Kelvin temperature to Celsius.
func KToC(k Kelvin) Celsius { return Celsius(float64(k) - float64(DiffK2C)) }

// CtoK converts a Kelvin temperature to Celsius.
func CToK(c Celsius) Kelvin { return Kelvin(float64(c) + float64(DiffK2C)) }
