package main

import (
	"ch02/tempconv"
	"fmt"
)

func main() {
	var k tempconv.Kelvin
	k = 100
	c := tempconv.KToC(k)
	f := tempconv.CToF(c)
	fmt.Printf("%s\n", c)
	fmt.Printf("%s\n", k)
	fmt.Printf("%s\n", f)

}
